package com.nrkei.microservices.car_rental_offer;

import com.nrkei.microservices.rapids_rivers.Packet;
import java.util.ArrayList;
import java.util.HashMap;

public class AbstractPacketReactor extends AbstractPacketListener {

    @Override
    protected void registerHeartbeat() {
        publish(helloPacket());
        new HeartbeatThread().start();
    }

    public class HeartbeatThread extends Thread {
        @Override
        public void run() {
            try {
                while (true) {
                    publish(heartbeatPacket());
                    Thread.sleep(20000);
                }
            } catch (Exception e) {
                throw new RuntimeException("Could not publish message:", e);
            }
        }

        private Packet heartbeatPacket() {
            HashMap<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("heartbeat", getServiceName());
            jsonMap.put("trace", new ArrayList<>());
            return new Packet(jsonMap);
        }
    }

    private Packet helloPacket() {
        HashMap<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("hello", getServiceName());
        jsonMap.put("trace", new ArrayList<>());
        return new Packet(jsonMap);
    }

    protected String getServiceName() {
        return getClass().getSimpleName();
    }

}
