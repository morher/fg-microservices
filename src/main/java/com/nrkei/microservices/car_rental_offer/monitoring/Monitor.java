package com.nrkei.microservices.car_rental_offer.monitoring;
/*
 * Copyright (c) 2016 by Fred George
 * May be used freely except for training; license required for training.
 * @author Fred George
 */

import com.nrkei.microservices.car_rental_offer.AbstractPacketListener;
import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;

// Understands the messages on an event bus
public class Monitor extends AbstractPacketListener {

    public static void main(String[] args) {
        new Monitor().start(args);
    }

    @Override
    protected void registerListeners() {
        river()
                .register(this);
    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        System.out.println(String.format(" [*] %s", warnings));
    }

    @Override
    public void onError(RapidsConnection connection, PacketProblems errors) {
        System.out.println(String.format(" [x] %s", errors));
    }
}
