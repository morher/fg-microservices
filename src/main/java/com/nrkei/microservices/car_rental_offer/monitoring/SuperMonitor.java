package com.nrkei.microservices.car_rental_offer.monitoring;

import com.nrkei.microservices.car_rental_offer.AbstractPacketListener;
import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;
import com.nrkei.microservices.rapids_rivers.River;
import java.util.function.Function;

public class SuperMonitor extends AbstractPacketListener {
    private Object followRequestId;

    public static void main(String[] args) {
        new SuperMonitor().start(args);
    }

    @Override
    protected void registerListeners() {
        river()
                .require("request-id")
                .interestedIn("offer", "member")
                .register(this);
    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        Object requestId = packet.get("request-id");
        Object member = packet.get("member");
        Object offer = packet.get("offer");

        if (followRequestId == null && offer == null && member == null) {
            followRequestId = requestId;

            System.out.println(" === Following request " + followRequestId + " === ");
        }
        if (followRequestId != null) {
            if (followRequestId.equals(requestId)) {
                System.out.println(packet.toJson());
            }
        }

    }

    public static class Logger implements River.PacketListener {
        private Function<Packet, String> func;

        public Logger(Function<Packet, String> func) {
            this.func = func;
        }

        @Override
        public void onPacket(Packet packet) {
            System.out.println(func.apply(packet));
        }

        @Override
        public void onError(RapidsConnection connection, PacketProblems errors) {
            // TODO Auto-generated method stub

        }

    }
}
