package com.nrkei.microservices.car_rental_offer.monitoring;

import com.nrkei.microservices.car_rental_offer.AbstractPacketListener;
import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.kohsuke.args4j.Option;

public class HeartbeatMonitor extends AbstractPacketListener {
    private Map<Object, Integer> serviceStatuses = new ConcurrentHashMap<>();

    @Option(name = "-listfile")
    private String filename;

    @Option(name = "-timeout")
    private int timeout = 30;

    @Option(name = "-list-freq")
    private int listToFileFreq = 10;

    public static void main(String[] args) {
        new HeartbeatMonitor().start(args);
    }

    public HeartbeatMonitor() {
        new HeartbeatDecrementThread().start();

        river()
                .require("hello")
                .register(this);

        river()
                .require("heartbeat")
                .register(this);

    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        Object service = packet.get("heartbeat");
        if (service == null) {
            service = packet.get("hello");
            System.out.println("Hi: " + service);
        }
        if (service != null) {
            serviceStatuses.put(service, timeout);
        }
    }

    protected void checkState() {
        for (Map.Entry<Object, Integer> entry : serviceStatuses.entrySet()) {
            Object service = entry.getKey();
            Integer countdown = entry.getValue();

            if (countdown > 0) {
                serviceStatuses.put(service, countdown - 1);

            } else {
                System.err.println("Oh NO!!!! " + service);
                publish(errorPacket(service));
                serviceStatuses.remove(service);
            }

        }
    }

    protected void dumpFile() {
        if (filename != null) {
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(filename));

                for (Map.Entry<Object, Integer> entry : serviceStatuses.entrySet()) {
                    String service = entry.getKey().toString();
                    writer.write(service);
                    writer.write("\n");
                }
                writer.close();

            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

    private Packet errorPacket(Object service) {
        HashMap<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("down", service);
        jsonMap.put("trace", new ArrayList<>());
        return new Packet(jsonMap);
    }

    public class HeartbeatDecrementThread extends Thread {
        @Override
        public void run() {
            try {
                int i = 0;
                while (true) {
                    checkState();
                    if (i % listToFileFreq == 0) {
                        dumpFile();
                    }
                    Thread.sleep(1000);
                }
            } catch (Exception e) {
                throw new RuntimeException("Could not publish message:", e);
            }
        }
    }
}
