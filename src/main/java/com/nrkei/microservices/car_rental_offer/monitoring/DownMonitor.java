package com.nrkei.microservices.car_rental_offer.monitoring;

import com.nrkei.microservices.car_rental_offer.AbstractPacketListener;
import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.kohsuke.args4j.Option;

public class DownMonitor extends AbstractPacketListener {
    static int d = 0;

    @Option(name = "-slack")
    private String slackUrl = "https://hooks.slack.com/services/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";

    public static void main(String[] args) throws Exception {
        new DownMonitor().start(args);
    }

    @Override
    protected void registerListeners() {
        river()
                .require("down")
                .register(this);
    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        sendMicroserviceDownAlert(packet.get("down").toString());
    }

    private static String[] dead = {
            "'E's not pinin'! 'E's passed on!",
            "This microservice is no more! He has ceased to be!",
            "'E's expired and gone to meet 'is maker!",
            "'E's a stiff! Bereft of life, 'e rests in peace!",
            "If you hadn't nailed 'im to the perch 'e'd be pushing up the daisies!",
            "'Is metabolic processes are now 'istory!",
            "'E's off the twig! 'E's kicked the bucket, 'e's shuffled off 'is mortal coil, run down the curtain and joined the bleedin' choir invisible!!",
            "THIS IS AN EX-MICROSERVICE" };

    private void sendMicroserviceDownAlert(String microservice) {
        if (d >= dead.length) {
            d = 0;
        }
        String json = "{" +
                "    \"text\": \"" + dead[d++] + "\"," +
                "    \"attachments\": [" +
                "        {" +
                "            \"color\": \"danger\",\r\n" +
                "            \"text\": \"MicroService: " + microservice + "\"" +
                "        }" +
                "    ]" +
                "}";
        sendAlert(json);
    }

    private void sendAlert(String json) {
        try {
            HttpClient httpclient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(slackUrl);

            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            List<NameValuePair> params = new ArrayList<NameValuePair>(2);

            // Execute and get the response.
            HttpResponse response = httpclient.execute(httpPost);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
    }

}
