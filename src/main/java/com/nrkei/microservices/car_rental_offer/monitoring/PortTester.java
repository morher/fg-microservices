package com.nrkei.microservices.car_rental_offer.monitoring;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;

public class PortTester {
    public static void main(String arg[]) {
        for (int i = 160; i < 1000; i++) {
            try {
                HttpClient httpclient = HttpClients.createDefault();
                HttpPost httpPost = new HttpPost("http://portquiz.net:" + i);

                // Execute and get the response.
                HttpResponse response = httpclient.execute(httpPost);

            } catch (Exception e) {
                // TODO Auto-generated catch block
                System.err.println("Close port: " + i + " (" + e.getMessage() + ")");
                continue;

            }
            System.out.println("Open port: " + i);
        }
    }
}
