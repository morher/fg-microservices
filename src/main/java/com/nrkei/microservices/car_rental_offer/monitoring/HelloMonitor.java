package com.nrkei.microservices.car_rental_offer.monitoring;

import com.nrkei.microservices.car_rental_offer.AbstractPacketListener;
import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.kohsuke.args4j.Option;

public class HelloMonitor extends AbstractPacketListener {

    @Option(name = "-slack")
    private String slackUrl = "https://hooks.slack.com/services/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";

    public static void main(String[] args) {
        new HelloMonitor().start(args);
    }

    @Override
    protected void registerListeners() {
        river()
                .require("hello")
                .register(this);
    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        sendMicroserviceHelloAlert(packet.get("hello").toString());
    }

    private void sendMicroserviceHelloAlert(String microservice) {
        String json = "{" +
                "    \"text\": \"Welcome\"," +
                "    \"attachments\": [" +
                "        {" +
                "            \"color\": \"good\",\r\n" +
                "            \"text\": \"MicroService: " + microservice + "\"" +
                "        }" +
                "    ]" +
                "}";
        sendAlert(json);
    }

    private void sendAlert(String json) {
        try {
            HttpClient httpclient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(slackUrl);

            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            List<NameValuePair> params = new ArrayList<NameValuePair>(2);

            // Execute and get the response.
            HttpResponse response = httpclient.execute(httpPost);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
    }

}
