package com.nrkei.microservices.car_rental_offer.need;

import com.nrkei.microservices.car_rental_offer.AbstractPacketReactor;
import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import org.kohsuke.args4j.Option;

public class UserNeed extends AbstractPacketReactor {

    @Option(name = "-user", aliases = "-u")
    private String userId = "Harry";

    @Option(name = "-min")
    private int min = 1;

    @Option(name = "-max")
    private int max = 60;

    private String lastUuid;

    public static void main(String[] args) throws Exception {
        new UserNeed().start(args);
    }

    @Override
    protected void registerListeners() {
        river()
                .requireValue("user-id", userId)
                .interestedIn("request-id")
                .register(this);
    }

    @Override
    protected void run() {
        try {
            while (true) {
                publish(needPacket());
                Thread.sleep((rnd(max - min) + min) * 1000);
            }

        } catch (Exception e) {
            throw new RuntimeException("Could not publish message:", e);
        }
    }

    private Packet needPacket() {
        String uuid = UUID.randomUUID().toString();
        System.out.println("\n\n\n === New need: " + uuid + " === \n");
        HashMap<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("need", "car_rental_offer");
        jsonMap.put("request-id", uuid);
        jsonMap.put("location", rndLocation());
        jsonMap.put("trace", new ArrayList<>());
        if (userId != null) {
            jsonMap.put("user-id", userId);
        }
        lastUuid = uuid;
        return new Packet(jsonMap);
    }

    static String[] LOCATIONS = { "Boston", "London", "Tokyo", "Oslo" };

    private static String rndLocation() {
        return LOCATIONS[rnd(LOCATIONS.length)];
    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        if (packet.get("request-id").equals(lastUuid)) {
            System.out.println(" [>] " + packet.toJson());
        }
    }

    @Override
    protected String getServiceName() {
        return super.getServiceName() + "(" + userId + ")";
    }

}