package com.nrkei.microservices.car_rental_offer.need;
/* 
 * Copyright (c) 2016 by Fred George
 * May be used freely except for training; license required for training.
 * @author Fred George
 */

import com.nrkei.microservices.car_rental_offer.AbstractPacketReactor;
import com.nrkei.microservices.rapids_rivers.Packet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

// Understands the requirement for advertising on a site
public class Need extends AbstractPacketReactor {

    private static int i = 1500;

    public static void main(String[] args) throws Exception {
        new Need().start(args);
    }

    public void run() {

        try {
            while (true) {
                publish(needPacket());
                Thread.sleep(5000);
            }
        } catch (Exception e) {
            throw new RuntimeException("Could not publish message:", e);
        }
    }

    private static Packet needPacket() {
        HashMap<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("need", "car_rental_offer");
        jsonMap.put("request-id", UUID.randomUUID());
        jsonMap.put("location", rndLocation());
        jsonMap.put("trace", new ArrayList<>());
        String userId = rndUser();
        if (userId != null) {
            jsonMap.put("user-id", userId);
        }

        return new Packet(jsonMap);
    }

    static String[] LOCATIONS = { "Boston", "London", "Tokyo", "Oslo" };

    private static String rndLocation() {
        return LOCATIONS[rnd(LOCATIONS.length)];
    }

    private static String[] USER_PART1 = { "blue", "happy", "angry", "quick", "sneaky", "hidden", "creamy", "soft", "forgiven", "soaky" };

    private static String[] USER_PART2 = { "Fox", "Dog", "Frog", "Boat", "Huricane", "President", "Horse", "Gingerbread" };

    private static String rndUser() {
        if (rnd(100) >= 80) {
            return null;
        }
        return rnd(USER_PART1) + rnd(USER_PART2) + rnd(100);
    }
}
