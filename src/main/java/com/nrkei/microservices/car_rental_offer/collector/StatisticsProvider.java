package com.nrkei.microservices.car_rental_offer.collector;

import com.nrkei.microservices.car_rental_offer.AbstractPacketReactor;
import com.nrkei.microservices.rapids_rivers.Packet;

public class StatisticsProvider extends AbstractPacketReactor {

    public static void main(String[] args) {
        new StatisticsProvider().start(args);
    }

    @Override
    protected void registerListeners() {
        river()
                .requireValue("", "")
                .register(this::onAcceptOfferPacket);
    }

    public void onAcceptOfferPacket(Packet packet) {

    }

    public static class UserStatistic {

    }
}
