package com.nrkei.microservices.car_rental_offer.solutions;

import com.nrkei.microservices.car_rental_offer.AbstractPacketReactor;
import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;

public class FreeGpsSolutionProvider extends AbstractPacketReactor {

    public static void main(String[] args) throws Exception {
        new FreeGpsSolutionProvider().start(args);
    }

    @Override
    protected void registerListeners() {
        river()
                .requireValue("need", "car_rental_offer")
                .requireValue("location", "London")
                .forbid("offer")
                .interestedIn("value", "trace")
                .register(this);
    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        packet.put("offer", "Free GPS in London");
        packet.put("value", 150);

        publish(packet);
    }

}
