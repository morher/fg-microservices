package com.nrkei.microservices.car_rental_offer.solutions;

import com.nrkei.microservices.car_rental_offer.AbstractPacketReactor;
import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;

public class FabolousSolutionProvider extends AbstractPacketReactor {

    public static void main(String[] args) {
        new FabolousSolutionProvider().start(args);
    }

    @Override
    protected void registerListeners() {
        river()
                .requireValue("need", "car_rental_offer")
                .requireValue("member", "true")
                .forbid("offer")
                .interestedIn("value", "trace")
                .register(this);
    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        System.out.println(String.format(" [*] %s", warnings));

        sleep((long) (Math.random() * 3000));

        packet.put("offer", "Free lunch included!");
        packet.put("value", 5000);
        publish(packet);
    }

}
