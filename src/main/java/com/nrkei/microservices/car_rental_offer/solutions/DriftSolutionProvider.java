package com.nrkei.microservices.car_rental_offer.solutions;

import com.nrkei.microservices.car_rental_offer.AbstractPacketReactor;
import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;

public class DriftSolutionProvider extends AbstractPacketReactor {

    public static void main(String[] args) {
        new DriftSolutionProvider().start(args);
    }

    @Override
    protected void registerListeners() {
        river()
                .requireValue("need", "car_rental_offer")
                .requireValue("location", "Tokyo")
                .forbid("offer")
                .interestedIn("value", "trace")
                .register(this);
    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        packet.put("offer", "10% on any Toyota in Tokyo");
        packet.put("value", 1500);

        publish(packet);
    }
}
