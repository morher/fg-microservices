package com.nrkei.microservices.car_rental_offer;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;

public class SolutionCollector extends AbstractPacketListener {

    Cache<Object, Packet> cache = CacheBuilder.newBuilder().build();

    public static void main(String[] args) {
        new SolutionCollector();
    }

    public SolutionCollector() {
        river()
                .requireValue("need", "car_rental_offer")
                .require("request-id")
                .require("offer", "value")
                .forbid("best-offer")
                .register(this);
    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        Object requestId = packet.get("request-id");
        Packet existingPacket = cache.getIfPresent(requestId);

        if (existingPacket == null || isBetter(packet, existingPacket)) {
            packet.put("best-offer", "");
            cache.put(requestId, packet);
            publish(packet);
        }

    }

    private boolean isBetter(Packet packet, Packet existingPacket) {
        Number newValue = (Number) packet.get("value");
        Number oldValue = (Number) existingPacket.get("value");

        return newValue != null
                && (oldValue == null
                        || newValue.doubleValue() > oldValue.doubleValue());
    }
}
