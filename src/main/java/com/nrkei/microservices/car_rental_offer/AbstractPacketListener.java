package com.nrkei.microservices.car_rental_offer;

import com.nrkei.microservices.rapids_rivers.Packet;
import com.nrkei.microservices.rapids_rivers.PacketProblems;
import com.nrkei.microservices.rapids_rivers.RapidsConnection;
import com.nrkei.microservices.rapids_rivers.River;
import com.nrkei.microservices.rapids_rivers.rabbit_mq.RabbitMqRapids;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetAddress;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

public class AbstractPacketListener implements River.PacketListener {

    @Option(name = "-host")
    private String host = "ln01.srv.h-dataservice.net";

    @Option(name = "-port")
    private String port = "465";

    private final RapidsConnection connection;

    public AbstractPacketListener() {
        connection = new RabbitMqRapids("monitor_in_java", host, port);
    }

    public final void start(String args[]) {
        try {
            new CmdLineParser(this).parseArgument(args);

            registerHeartbeat();
            registerListeners();
            run();

        } catch (Exception e) {
            throw new RuntimeException("Program failed...", e);

        }
    }

    protected void registerHeartbeat() {

    }

    protected void registerListeners() {

    }

    protected void run() throws Exception {

    }

    protected void publish(Packet packet) {
        addTrace(packet);

        String jsonMessage = packet.toJson();
        System.out.println(String.format(" [<] %s", jsonMessage));

        connection.publish(jsonMessage);
    }

    protected River river() {
        return new River(connection);
    }

    protected void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void packet(RapidsConnection connection, Packet packet, PacketProblems warnings) {
        onPacket(packet);
    }

    @Override
    public void onError(RapidsConnection connection, PacketProblems errors) {
    }

    @Override
    public void onPacket(Packet packet) {

    }

    private void addTrace(Packet packet) {

        HashMap<Object, Object> traceEntry = new HashMap<>();
        try {
            traceEntry.put("source", InetAddress.getLocalHost().getHostName() + ":" + this.getClass().getSimpleName());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        traceEntry.put("timestamp", DateTimeFormatter.ofPattern("HH:mm:ss.SSS").withZone(ZoneId.systemDefault()).format(Instant.now()));

        List trace = packet.getList("trace");
        if (trace == null) {
            trace = new ArrayList();
            packet.put("trace", trace);
        }
        trace.add(traceEntry);
    }

    public static int rnd(int max) {
        return (int) (Math.random() * max);
    }

    public static String rnd(String[] arr) {
        return arr[rnd(arr.length)];
    }

}
