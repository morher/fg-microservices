package com.nrkei.microservices.car_rental_offer;

import java.util.Optional;

public class CommonStarter {

    public static void main(String[] args) {
        String service = args[0];

    }

    private Optional<AbstractPacketListener> getService(String packageName, String serviceName) {
        Class<?> clazz;
        try {
            clazz = getClass().getClassLoader().loadClass(packageName + "." + serviceName);

            Object obj = clazz.newInstance();

            return Optional.of((AbstractPacketListener) obj);

        } catch (ClassNotFoundException e) {
            return Optional.empty();

        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException("Ojda!", e);
        }
    }
}
